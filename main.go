package main

import (
	"palworld/common"
	"palworld/router"
)

//go:generate statik -src=./static -dest=./ -f
func main() {
	common.Init()
	common.Pal = common.NewPal()
	go common.Pal.BakSave()
	go common.Pal.RunSafe()
	r := router.NewRouter()
	r.Run()
}
