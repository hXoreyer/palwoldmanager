package common

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

var jwtKey = []byte("KeingJwtSecret")

// 定义token的Claims
type Claims struct {
	UserId uint
	jwt.StandardClaims
}

// 登录成功之后就调用这个方法来释放token
func ReleaseToken(user Info) (string, error) {
	//定义token的过期时间:7天
	expirationTime := time.Now().Add(7 * 24 * time.Hour)
	claims := &Claims{
		UserId: 1,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(), //token发放的时间
			Issuer:    "admin",           //发放人
			Subject:   "user token",      //主题
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//使用jwt密钥来生成token
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
