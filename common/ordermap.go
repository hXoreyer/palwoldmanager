package common

type OrderMapFunc func(key, value any) bool

type OrderedMap struct {
	keys []any
	m    map[any]any
}

func NewOrderMap() *OrderedMap {
	return &OrderedMap{
		keys: make([]any, 0),
		m:    make(map[any]any),
	}
}

func (omap *OrderedMap) Len() int {
	return len(omap.keys)
}

func (omap *OrderedMap) Store(key any, value any) {
	if _, ok := omap.m[key]; !ok {
		omap.keys = append(omap.keys, key)
	}
	omap.m[key] = value
}

func (omap *OrderedMap) Load(key any) any {
	return omap.m[key]
}

func (omap *OrderedMap) ForEach(fn OrderMapFunc) {
	for _, v := range omap.keys {
		if !fn(v, omap.m[v]) {
			break
		}
	}
}

func (omap *OrderedMap) Swap(i, j int) {
	omap.keys[i], omap.keys[j] = omap.keys[j], omap.keys[i]
}
