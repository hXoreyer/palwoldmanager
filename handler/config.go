package handler

import (
	"palworld/common"

	"github.com/gin-gonic/gin"
)

type configResp struct {
	Key   string `json:"key"`
	Label string `json:"label"`
	Value any    `json:"value"`
}

type configReq struct {
	Datas []configResp `json:"Datas"`
}

func GetConfig(c *gin.Context) {
	config := common.NewConfig()

	res := make([]configResp, config.Len()-1)
	i := 0
	config.ForEach(func(key, value any) bool {
		if key != "AdminPassword" {
			res[i] = configResp{
				Key:   key.(string),
				Label: config.GetCN(key.(string)),
				Value: value,
			}
			i++
		}
		return true
	})

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "获取成功",
		"data": res,
	})
}

func SetConfig(c *gin.Context) {
	datas := configReq{}
	c.ShouldBindJSON(&datas)
	config := common.NewConfig()

	for _, v := range datas.Datas {
		config.Set(v.Key, v.Value)
	}

	config.Save()

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "保存成功",
	})
}
