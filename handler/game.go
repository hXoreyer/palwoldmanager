package handler

import (
	"fmt"
	"palworld/common"

	"github.com/gin-gonic/gin"
)

func Update(c *gin.Context) {
	go common.Pal.Update()
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "正在重启服务器...",
	})
}

func StopGame(c *gin.Context) {
	go common.Pal.ShutDown()
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "停止服务器成功",
	})
}

func StartGame(c *gin.Context) {
	go common.Pal.RunSafe()
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "启动服务器成功",
	})
}
func RestartGame(c *gin.Context) {
	go common.Pal.RunSafe()
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "重启动服务器成功",
	})
}

func IsRunning(c *gin.Context) {
	b_running := common.Pal.Running()
	if b_running {
		c.JSON(200, gin.H{
			"code": 200,
			"msg":  "running",
		})
		return
	} else {
		c.JSON(200, gin.H{
			"code": 200,
			"msg":  "stopped",
		})
	}
}

func GetMem(c *gin.Context) {
	mem := common.Pal.GetMem()
	c.JSON(200, gin.H{
		"code": 200,
		"data": fmt.Sprintf("%f%%", mem),
	})
}

func GetCpu(c *gin.Context) {
	mem := common.Pal.GetCpu()
	c.JSON(200, gin.H{
		"code": 200,
		"data": fmt.Sprintf("%f%%", mem),
	})
}

func GetYamlConfig(c *gin.Context) {
	info := common.GetInfo()

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "获取成功",
		"data": info,
	})
}

func UpdateConfig(c *gin.Context) {
	info := &common.Info{}
	c.ShouldBindJSON(info)

	rinf := common.GetInfo()
	info.UserName = rinf.UserName
	info.PassWord = rinf.PassWord
	info.Server = rinf.Server

	common.SetInfo(info)
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "保存成功",
	})
}

func GetPlayers(c *gin.Context) {
	players, err := common.NewRcon().GetPlayers()
	if err != nil {
		c.JSON(200, gin.H{
			"code":  203,
			"msg":   "获取玩家失败",
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"code":    200,
		"msg":     "获取成功",
		"players": players,
	})
}

func Ban(c *gin.Context) {
	uid := c.Query("uid")
	_, err := common.NewRcon().Ban(uid)
	if err != nil {
		c.JSON(200, gin.H{
			"code":  203,
			"msg":   "操作失败",
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "操作成功",
	})
}

func Kick(c *gin.Context) {
	uid := c.Query("uid")
	_, err := common.NewRcon().Kick(uid)
	if err != nil {
		c.JSON(200, gin.H{
			"code":  203,
			"msg":   "操作失败",
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "操作成功",
	})
}

func Broadcast(c *gin.Context) {
	var msg struct {
		Data string `json:"msg"`
	}

	c.ShouldBindJSON(&msg)

	common.NewRcon().BroadCast(msg.Data)

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "消息发送成功",
	})
}

func GetBaks(c *gin.Context) {
	fs := common.Pal.GetBaks()

	type ps struct {
		Name string `json:"name"`
		Path string `json:"path"`
	}
	pos := make([]ps, len(fs))
	info := common.GetInfo()
	for k, v := range fs {
		pos[k] = ps{
			Name: v,
			Path: fmt.Sprintf("%s/bak/%s", info.PalPath, v),
		}
	}

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "获取成功",
		"baks": pos,
	})
}

func DelBak(c *gin.Context) {
	name := c.Param("name")
	if err := common.Pal.DelBak(name); err != nil {
		c.JSON(200, gin.H{
			"code":  202,
			"msg":   "删除失败",
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}
