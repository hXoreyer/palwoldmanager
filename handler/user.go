package handler

import (
	"palworld/common"

	"github.com/gin-gonic/gin"
)

func Login(c *gin.Context) {
	info := common.Info{}
	c.ShouldBindJSON(&info)

	real := common.GetInfo()
	if info.UserName != real.UserName {
		c.JSON(200, gin.H{
			"code": 500,
			"msg":  "账号错误",
		})
		return
	} else if info.PassWord != real.PassWord {
		c.JSON(200, gin.H{
			"code": 500,
			"msg":  "密码错误",
		})
		return
	}

	c.JSON(200, gin.H{
		"code":  200,
		"msg":   "登录成功",
		"token": common.Pal.Token(),
	})
}

func ResetPassword(c *gin.Context) {
	info := common.Info{}
	c.ShouldBindJSON(&info)

	common.SetInfo(&info)
	c.JSON(200, gin.H{
		"code": 200,
		"msg":  "修改成功",
	})
}
