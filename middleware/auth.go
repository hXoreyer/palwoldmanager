package middleware

import (
	"palworld/common"

	"github.com/gin-gonic/gin"
)

func Auth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth := ctx.GetHeader("Auth")
		if auth == common.Pal.Token() {
			ctx.Next()
			return
		}
		ctx.JSON(200, gin.H{
			"code": 401,
			"msg":  "身份验证过期",
		})
		ctx.Abort()
	}
}
